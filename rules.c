#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#define RADIX 10
#define OPTS "bdfinrk:"
#define IGNSPACES 'b'
#define DICTIONARY 'd'
#define IGNCASE 'f'
#define PRINTABLE 'i'
#define COLUMNS 'k'
#define NUMERICAL 'n'
#define REVERSE 'r'
#define UNKNOWN '?'

static int bFlag = 0;
static int dFlag = 0;
static int fFlag = 0;
static int iFlag = 0;
static int kFlag = 0;
static int kVal = 0;
static int nFlag = 0;
static int rFlag = 0;

void SetOpts(int opt, char *optarg) {
   switch (opt) {
   case IGNSPACES:
      bFlag = 1;
      break;
   case DICTIONARY:
      dFlag = 1;
      break;
   case IGNCASE:
      fFlag = 1;
      break;
   case PRINTABLE:
      iFlag = 1;
      break;
   case COLUMNS:
      kFlag = 1;
      kVal = strtol(optarg, NULL, RADIX);

      if (!kVal) {
         fprintf(stderr, "-k option requires an integer argument.\n");
         exit(EXIT_FAILURE);
      }
      break;
   case NUMERICAL:
      nFlag = 1;
      break;
   case REVERSE:
      rFlag = 1;
      break;
   case UNKNOWN:
      fprintf(stderr, "Usage: SuperSort -%s file1 ...\n",
       OPTS);
      exit(EXIT_FAILURE);
   }
}

char *GetLine(void const *vstr) {
   char const **str = (char const **) vstr;
   char *line = calloc(sizeof(char), strlen(*str) + 1);

   line = strcpy(line, *str);
   return line;
}
   

int BasicCmp(char *str1, char *str2) {
   return strcmp(str1, str2);
}

int IgnoreBlanks(char *str1, char *str2) {
   int index1, index2, retVal;

   index1 = index2 = 0;   

   while ((str1[index1] == ' ') || (str1[index1] == '\t'))
      index1++;

   while ((str2[index2] == ' ') || (str2[index2] == '\t'))
      index2++;

   retVal = strcmp(&(str1[index1]), &(str2[index2]));

   if (!retVal)
      retVal = strlen(str2) - strlen(str1);

   return retVal;
}

int DictOrder(char *str1, char *str2) {
   int i1, i2, retVal;

   i1 = i2 = 0;

   while ((str1[i1] != '\0') && (str2[i2] != '\0')) {
      if ((!isalnum(str1[i1])) || (!isalnum(str2[i2]))) {
         if (!isalnum(str1[i1]))
            i1++;
         else
            i2++;
      }
      else {
         if (str1[i1] == str2[i2]) {
            i1++;
            i2++;
         }
         else {
            retVal = str1[i1] - str2[i2];
            return retVal;
         }
      }
   }
   
   if ((str1[i1] == '\0') || (str2[i2] == '\0'))
      retVal = strlen(str2) - strlen(str1);
   else
      retVal = str1[i1] - str2[i2];

   return retVal;
}

int IgnoreCase(char *str1, char *str2) {
   int retVal;

   if ((retVal = strcasecmp(str1, str2))) 
      return retVal;

   return strcmp(str1, str2);
}

int PrintOnly(char *str1, char *str2) {
   int i1, i2, retVal;

   i1 = i2 = 0;

   while ((str1[i1] != '\0') && (str2[i2] != '\0')) {
      if ((!isprint(str1[i1])) || (!isprint(str2[i2]))) {
         if (!isprint(str1[i1]))
            i1++;
         else
            i2++;
      }
      else {
         if (str1[i1] == str2[i2]) {
            i1++;
            i2++;
         }
         else {
            retVal = str1[i1] - str2[i2];
            return retVal;
         }
      }
   }

   retVal = str1[i1] - str2[i2];
   return retVal;
}

int NumOrder(char *str1, char *str2) {
   char *endPtr1 = str1;
   char *endPtr2 = str2;
   float val1, val2;
   float retVal;

   val1 = strtof(str1, &endPtr1);
   val2 = strtof(str2, &endPtr2);
   
   if (endPtr1 && endPtr2) {
      retVal = val1 - val2;
      if (!retVal)
         retVal = strlen(str2) - strlen(str1);
   }
   else
      retVal = strcmp(str1, str2);

   if (retVal > 0)
      retVal = 1;
   else if (retVal < 0)
      retVal = -1;

   return retVal;
}

int Compare(void const *vstr1, void const *vstr2) {
   char *str1 = GetLine(vstr1);
   char *str2 = GetLine(vstr2);
   char *temp1 = str1;
   char *temp2 = str2;
   int retVal = 0;
   
   if(kFlag) {
      int col = 1;

      while ((col < kVal) && str1) {
         col++;
         str1 = strtok(str1, " \t");
         if (str1)
            str1 += (strlen(str1) + 1);
         else {
            col = kVal;
            str1 = temp1;
            retVal = -1;
         }
      }
      
      col = 1;
      
      while ((col<kVal) && str2) {
         col++;
         str2 = strtok(str2, " \t");
         if (str2)
            str2 += ((strlen(str2) + 1));
         else {
            col = kVal;
            str2 = temp1;
            retVal = 1;
         }
      }
   }

   if (bFlag && !retVal)
      retVal = IgnoreBlanks(str1, str2);
   else if (dFlag && !retVal)
      retVal = DictOrder(str1, str2);
   else if (fFlag && !retVal)
      retVal = IgnoreCase(str1, str2);
   else if (iFlag && !retVal)
      retVal = PrintOnly(str1, str2);
   else if (nFlag && !retVal)
      retVal = NumOrder(str1, str2);
   else if (!retVal)
      retVal = BasicCmp(str1, str2);

   free(temp1);
   free(temp2);
   return retVal;  
}

void PrintArr(char **lines, int size) {
   int index = 0;

   if (rFlag) {
      while (index != (size--))
         printf("%s\n", lines[size]);
   }
   else {
      while (index != size) {
         printf("%s\n", lines[index]);
         index++;
      }
   }
}


