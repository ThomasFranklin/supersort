#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "linereader.h"
#include "rules.h"

#define OPTS "bdfik:nr"

int main(int argc, char **argv) {
   char **lines = calloc(sizeof(char *), 1);
   int size = 0;
   FILE *file;
   int opt, index;

   while ((opt = getopt(argc, argv, OPTS)) != -1)
      SetOpts(opt, optarg);

   index = optind;

   if (index == argc)
      lines = ReadFile(stdin, lines, &size);
   else {
      for (; index < argc; index++) {
         file = fopen(argv[index], "r");
         if (file) { 
            lines = ReadFile(file, lines, &size);
         }
         else {
            fprintf(stderr, "Could not read file %s\n",
             argv[index]);
            exit(EXIT_FAILURE);
         }
      }
   }   

   qsort(lines, size, sizeof(char *), Compare);

   PrintArr(lines, size);

   FreeAll(lines, size);

   return 0; 
}
