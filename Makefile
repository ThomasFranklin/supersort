CC = gcc
CFLAGS = -Wall -Werror -O3
OBJS = linereader.o rules.o
HEADS = linereader.h rules.h
MAIN = SuperSort

all : $(MAIN)

$(MAIN) : $(OBJS) $(HEADS) supersort.c
	$(CC) $(CFLAGS) -o $(MAIN) supersort.c $(OBJS)

linereader.o : linereader.c linereader.h
	$(CC) $(CFLAGS) -c linereader.c

rules.o : rules.c rules.h
	$(CC) $(CFLAGS) -c rules.c
