#include <stdlib.h>
#include <stdio.h>

char *ReadLine(FILE *file) {
   int size = 1;
   char *line = calloc(sizeof(char), size);
   char curChar;

   while ((curChar = fgetc(file)) != EOF) {
      line = realloc(line, sizeof(char) * size);

      if (curChar == '\n') {
         line[size - 1] = '\0';
         return line;
      }

      line[size - 1] = curChar;
      size++;
   }

   free(line);
   return NULL;
}

char **ReadFile(FILE *file, char **fileArr, int *size) {
   char *line = ReadLine(file);

   while (line) {
      fileArr[*size] = line;
      (*size)++;
      line = ReadLine(file);
      fileArr = realloc(fileArr, sizeof(char *) * (*size + 1));
   }

   return fileArr;
}

void FreeAll(char **lines, int size) {
   int index = 0;

   while (index < size) {
      free(lines[index]);
      index++;
   }

   free(lines);
}
